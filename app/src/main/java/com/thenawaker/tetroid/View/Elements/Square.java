/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.thenawaker.tetroid.View.Elements;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import android.graphics.Color;
import android.opengl.GLES20;

import com.thenawaker.tetroid.View.MyGLRenderer;

//Dessiner un carré

public class Square {
    public final static float SquareSize = 2f;
    /* Le vertex shader avec la définition de gl_Position et les variables utiles au fragment shader
        Attention pas de variables in par défaut
        Il faut tout définir par des uniform ou de attribute !!!!
     */
    private final static String vertexShaderCode =
            "uniform mat4 uMVPMatrix;" + // La matrice PxVxM transmise par le CPU
                    "attribute vec4 vPosition;" + // La position transmise par le CPU (pas de variable prédéfinie)
                    "uniform vec4 vCouleur;"+ // La couleur
                    "varying vec4 vCouleur2;"+ // Pour transmettre au fragment shader
                    "void main() {" +
                    "gl_Position = uMVPMatrix * vPosition;" +
                    "vCouleur2 = vCouleur;"+
                    "}";

    private final static String fragmentShaderCode =
            "precision mediump float;" + // pour définir la taille d'un float
                    "varying vec4 vCouleur2;"+ // on récupère la couleur interpolée
                    "void main() {" +
                    "  gl_FragColor = vCouleur2;" +
                    "}";

    /* les déclarations pour l'équivalent des VBO */

    private static FloatBuffer vertexBuffer; // Pour le buffer des coordonnées des sommets du carré
    private static ShortBuffer indiceBuffer; // Pour le buffer des indices

    /* les déclarations pour les shaders
    Identifiant du programme et pour les variables attribute ou uniform
     */
    private static int IdProgram; // identifiant du programme pour lier les shaders
    private static int IdPosition; // idendifiant (location) pour transmettre les coordonnées au vertex shader
    private static int IdCouleur; // identifiant (location) pour transmettre les couleurs
    private static int IdMVPMatrix; // identifiant (location) pour transmettre la matrice PxVxM

    static final int COORDS_PER_VERTEX = 3; // nombre de coordonnées par vertex

    /* Attention au repère au niveau écran (x est inversé)
     Le tableau des coordonnées des sommets
     Oui ce n'est pas joli avec 0.2 en dur ....
     */
    static float squareCoords[] = {
            -SquareSize/2f,   SquareSize/2f, 0.0f,
            -SquareSize/2f,  -SquareSize/2f, 0.0f,
            SquareSize/2f,  -SquareSize/2f, 0.0f,
            SquareSize/2f,   SquareSize/2f, 0.0f };

    // Le carré est dessiné avec 2 triangles
    private final static short Indices[] = { 0, 1, 2, 0, 2, 3 };

    private final static int vertexStride = COORDS_PER_VERTEX * 4; // le pas entre 2 sommets : 4 bytes per vertex

    public Square() {
        // initialisation du buffer pour les vertex (4 bytes par float)
        ByteBuffer bb = ByteBuffer.allocateDirect(squareCoords.length * 4);
        bb.order(ByteOrder.nativeOrder());
        vertexBuffer = bb.asFloatBuffer();
        vertexBuffer.put(squareCoords);
        vertexBuffer.position(0);

        // initialisation du buffer des indices
        ByteBuffer dlb = ByteBuffer.allocateDirect(Indices.length * 2);
        dlb.order(ByteOrder.nativeOrder());
        indiceBuffer = dlb.asShortBuffer();
        indiceBuffer.put(Indices);
        indiceBuffer.position(0);

        /* Chargement des shaders */
        int vertexShader = MyGLRenderer.loadShader(
                GLES20.GL_VERTEX_SHADER,
                vertexShaderCode);
        int fragmentShader = MyGLRenderer.loadShader(
                GLES20.GL_FRAGMENT_SHADER,
                fragmentShaderCode);

        IdProgram = GLES20.glCreateProgram();             // create empty OpenGL Program
        GLES20.glAttachShader(IdProgram, vertexShader);   // add the vertex shader to program
        GLES20.glAttachShader(IdProgram, fragmentShader); // add the fragment shader to program
        GLES20.glLinkProgram(IdProgram);                  // create OpenGL program executables
    }

    /* La fonction Display */
    public static void draw(float[] mvpMatrix,int color) {
        // Add program to OpenGL environment
        GLES20.glUseProgram(IdProgram);

        // get handle to vertex shader's vPosition member et vCouleur member
        IdPosition = GLES20.glGetAttribLocation(IdProgram, "vPosition");

        /* Activation des Buffers */
        GLES20.glEnableVertexAttribArray(IdPosition);

        /* Lecture des Buffers */
        GLES20.glVertexAttribPointer(
                IdPosition, COORDS_PER_VERTEX,
                GLES20.GL_FLOAT, false,
                vertexStride, vertexBuffer);


        // get handle to shape's transformation matrix
        IdMVPMatrix = GLES20.glGetUniformLocation(IdProgram, "uMVPMatrix");
        MyGLRenderer.checkGlError("glGetUniformLocation");

        // Apply the projection and view transformation
        GLES20.glUniformMatrix4fv(IdMVPMatrix, 1, false, mvpMatrix, 0);
        MyGLRenderer.checkGlError("glUniformMatrix4fv");

        IdCouleur = GLES20.glGetUniformLocation(IdProgram, "vCouleur");
        MyGLRenderer.checkGlError("glGetUniformLocation");

        // Apply the projection and view transformation
        GLES20.glUniform4fv(IdCouleur, 1, new float[]{Color.red(color)/255f, Color.green(color)/255f, Color.blue(color)/255f, Color.alpha(color)/255f}, 0);
        MyGLRenderer.checkGlError("glUniform4fv");


        // Draw the square
        GLES20.glDrawElements(
                GLES20.GL_TRIANGLES, Indices.length,
                GLES20.GL_UNSIGNED_SHORT, indiceBuffer);


        // Disable vertex array
        GLES20.glDisableVertexAttribArray(IdPosition);

    }

}