/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.thenawaker.tetroid.View;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.opengl.GLSurfaceView;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.WindowManager;

import com.thenawaker.tetroid.Models.Game;
import com.thenawaker.tetroid.Models.Grid;
import com.thenawaker.tetroid.Models.Tetrimino;
import com.thenawaker.tetroid.View.Elements.Square;

/* La classe MyGLSurfaceView avec en particulier la gestion des événements
  et la création de l'objet renderer

*/


/* On va dessiner un carré qui peut se déplacer grace à une translation via l'écran tactile */

public class MyGLSurfaceView extends GLSurfaceView {

    /* Un attribut : le renderer (GLSurfaceView.Renderer est une interface générique disponible) */
    /* MyGLRenderer va implémenter les méthodes de cette interface */

    private final MyGLRenderer mRenderer;
    private Game game;

    public MyGLSurfaceView(Context context,Game game) {
        super(context);
        this.game = game;

        // Création d'un context OpenGLES 2.0
        setEGLContextClientVersion(2);

        // Création du renderer qui va être lié au conteneur View créé

        mRenderer = new MyGLRenderer(getBarHeight());
        setRenderer(mRenderer);
        mRenderer.setGame(game);

        // Option pour indiquer qu'on redessine uniquement si les données changent
        setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);
    }

    private int getBarHeight(){
        Resources resources = this.getResources();
        int resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android");
        if (resourceId > 0) {
            return resources.getDimensionPixelSize(resourceId);
        }
        return 0;
    }
    /* Comment interpréter les événements sur l'écran tactile */
    @Override
    public boolean onTouchEvent(MotionEvent e) {
            System.out.println("TETRISX :"+e.getX());
            System.out.println("TETRISY :"+e.getY());
            float posx = (game.getCurrent().getBlockx()+game.getCurrent().getShape().getFirstCol())*mRenderer.getSquareSize();
            float posx_right = (game.getCurrent().getBlockx()+game.getCurrent().getSize())*mRenderer.getSquareSize();
            float posy_bottom = (game.getCurrent().getBlocky()+game.getCurrent().getSize())*mRenderer.getSquareSize();
            float posy = game.getCurrent().getBlocky()*mRenderer.getSquareSize();
        System.out.println("TETRISPosX :"+posx);
        System.out.println("TETRISPosXX :"+posx_right);
        System.out.println("TETRISPosY :"+posy);
        System.out.println("TETRISPosYY :"+posy_bottom);
        switch (e.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    if(e.getX()<posx){
                        game.action(Game.Action.MOVELEFT);
                    }else if(e.getX()>posx_right){
                        game.action(Game.Action.MOVERIGHT);
                    }else{
                        if(e.getY()>=posy && e.getY()<=posy_bottom){
                            game.action(Game.Action.ROTATE);
                        }else{
                            game.action(Game.Action.MOVEDOWN);
                        }
                    }
                    break;

                case MotionEvent.ACTION_UP:
                    game.action(Game.Action.NOTHING);
                    break;
            }

        return true;
    }
}
