package com.thenawaker.tetroid.View.Elements;

import android.opengl.Matrix;

import com.thenawaker.tetroid.Models.Tetrimino;

/**
 * Created by thenawaker on 19/04/2016.
 */
public class TetriminoView implements Cloneable {
    private Tetrimino tetrimino;

    public TetriminoView(Tetrimino tetrimino){
        this.tetrimino = tetrimino;
    }

    public void draw(float[] matrix,float[] matrixMVP){
        float[] scratch = new float[matrix.length];
        for(int i=0;i<tetrimino.getShape().shape.length;i++){
            for(int j=0;j<tetrimino.getShape().shape[i].length;j++){
                if(tetrimino.getShape().shape[i][j]=='X') {
                    if(tetrimino.getBlocky()+i>=0) {
                        float[] tmp = matrix.clone();
                        Matrix.translateM(tmp, 0, Square.SquareSize * j, -Square.SquareSize * i, 0);
                        Matrix.multiplyMM(scratch, 0, matrixMVP, 0, tmp, 0);
                        Square.draw(scratch, tetrimino.getColor());
                    }
                }
            }
        }
    }

    public void drawMiniature(float[] matrix,float[] matrixMVP){
        try {
            float[] scratch = new float[16];
            for (int i = 0; i < tetrimino.getShape().shape.length; i++) {
                for (int j = 0; j < tetrimino.getShape().shape[i].length; j++) {
                    if (tetrimino.getShape().shape[i][j] == 'X') {
                        float[] tmp = matrix.clone();
                        Matrix.translateM(tmp, 0, Square.SquareSize * j, -Square.SquareSize * i, 0);
                        Matrix.multiplyMM(scratch, 0, matrixMVP, 0, tmp, 0);
                        Square.draw(scratch, tetrimino.getColor());
                    }
                }
            }
        }catch(Exception e){
            System.out.println(tetrimino.getShapeId());
        }
    }

    public void setTetrimino(final Tetrimino tetrimino){
        this.tetrimino=tetrimino;
    }

    public Tetrimino getTetrimino(){
        return tetrimino;
    }
}
