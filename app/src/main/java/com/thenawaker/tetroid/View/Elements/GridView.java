package com.thenawaker.tetroid.View.Elements;

import android.graphics.Color;
import android.opengl.Matrix;

import com.thenawaker.tetroid.Models.Grid;
import com.thenawaker.tetroid.Models.Tetrimino;

/**
 * Created by thenawaker on 20/04/16.
 */
public class GridView{
    private final Grid grid;

    public GridView(Grid grid){
        this.grid = grid;
    }

    public void draw(float[] model,float[] mvpMatrix){
        float[] scratch = new float[16];
        for(int i=0;i<grid.gridHeight;i++){
            for(int j=0;j<grid.gridWidth;j++){
                float[] tmp = model.clone();
                Matrix.translateM(tmp, 0, Square.SquareSize * j, -Square.SquareSize * i, 0);
                Matrix.multiplyMM(scratch, 0, mvpMatrix, 0, tmp, 0);
                if(grid.getGrid()[i][j] != Tetrimino.ShapeID.X)
                    Square.draw(scratch, Tetrimino.getShapeList().get(grid.getGrid()[i][j]).color);
                else
                    Square.draw(scratch, Color.BLACK);
            }
        }
    }

    public Grid getGrid(){
        return grid;
    }
}
