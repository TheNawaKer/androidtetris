/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.thenawaker.tetroid.View;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.util.Log;

import com.thenawaker.tetroid.Models.Game;
import com.thenawaker.tetroid.View.Elements.GameView;
import com.thenawaker.tetroid.View.Elements.Square;

/* MyGLRenderer implémente l'interface générique GLSurfaceView.Renderer */

public class MyGLRenderer implements GLSurfaceView.Renderer {

    private static final String TAG = "MyGLRenderer";
    private GameView gameView;
    private Square square;
    public final float SquareGridNumber = 14f;
    private float squareSize;

    // Les habituelles matrices Model/View/Projection

    private final float[] mMVPMatrix = new float[16];
    private final float[] mProjectionMatrix = new float[16];
    private final float[] mViewMatrix = new float[16];
    private final float[] mModelMatrix = new float[16];

    private int width;
    private int height;
    private int barHeight;

    public int getWidth(){
        return width;
    }

    public int getHeight(){
        return height;
    }

    public float getSquareSize(){
        return squareSize;
    }

    public MyGLRenderer(int barHeight){
        this.barHeight = barHeight;
    }    /* Première méthode équivalente à la fonction init en OpenGLSL */
    @Override
    public void onSurfaceCreated(GL10 unused, EGLConfig config) {

        // la couleur du fond d'écran
        GLES20.glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        square = new Square();//on init la classe square
    }

    /* Deuxième méthode équivalente à la fonction Display */
    @Override
    public void onDrawFrame(GL10 unused) {
        float[] scratch = new float[16]; // pour stocker une matrice

        // glClear rien de nouveau on vide le buffer de couleur et de profondeur */
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);

        /* on utilise une classe Matrix (similaire à glm) pour définir nos matrices P, V et M*/

        /* Pour le moment on va utiliser une projection orthographique
           donc View = Identity
         */

        /*pour positionner la caméra mais ici on n'en a pas besoin*/

       // Matrix.setLookAtM(mViewMatrix, 0, 0, 0, -3, 0f, 0f, 0f, 0f, 1.0f, 0.0f);
        Matrix.setIdentityM(mViewMatrix, 0);

        // Calculate the projection and view transformation
        Matrix.multiplyMM(mMVPMatrix, 0, mProjectionMatrix, 0, mViewMatrix, 0);

        Matrix.setIdentityM(mModelMatrix, 0);
        Matrix.translateM(mModelMatrix, 0, squareSize/Square.SquareSize, height+(barHeight!=0 ? barHeight-4 : 0), 0);
        //Matrix.translateM(mModelMatrix, 0, 0, 0, 0);

        Matrix.scaleM(mModelMatrix, 0, squareSize/Square.SquareSize, squareSize/Square.SquareSize, 1);

        //Matrix.multiplyMM(scratch, 0, mMVPMatrix, 0, mModelMatrix, 0);
        /* on appelle la méthode dessin du carré élémentaire */
        gameView.draw(mModelMatrix, mMVPMatrix);
    }

    /* équivalent au Reshape en OpenGLSL */
    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height) {
        /* ici on aurait pu se passer de cette méthode et déclarer
        la projection qu'à la création de la surface !!
         */
        this.width = width;
        this.height = height-barHeight;
        this.squareSize = (float)width/SquareGridNumber;
        GLES20.glViewport(0, 0, this.width, this.height+barHeight);
        Matrix.orthoM(mProjectionMatrix, 0, 0, this.width, 0, this.height, -1, 1);

    }

    /* La gestion des shaders ... */
    public static int loadShader(int type, String shaderCode){

        // create a vertex shader type (GLES20.GL_VERTEX_SHADER)
        // or a fragment shader type (GLES20.GL_FRAGMENT_SHADER)
        int shader = GLES20.glCreateShader(type);

        // add the source code to the shader and compile it
        GLES20.glShaderSource(shader, shaderCode);
        GLES20.glCompileShader(shader);

        return shader;
    }

    /* pour gérer les erreurs ...*/
    public static void checkGlError(String glOperation) {
        int error;
        while ((error = GLES20.glGetError()) != GLES20.GL_NO_ERROR) {
            Log.e(TAG, glOperation + ": glError " + error);
            throw new RuntimeException(glOperation + ": glError " + error);
        }
    }

    public void setGame(Game game) {
        this.gameView = new GameView(game);
        game.addObserver(this.gameView);
    }
}
