package com.thenawaker.tetroid.View.Elements;

import android.opengl.Matrix;

import com.thenawaker.tetroid.Models.Game;
import com.thenawaker.tetroid.Models.Grid;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by thenawaker on 16/05/2016.
 */
public class GameView implements Observer {
    private TetriminoView currentTetrimino;
    private TetriminoView nextTetrimino;
    private GridView gridView;
    private Game game;
    public GameView(Game game){
        this.game = game;
        gridView = new GridView(game.getGrid());
        currentTetrimino = new TetriminoView(game.getCurrent());
        nextTetrimino = new TetriminoView(game.getNext());
    }

    public void draw(float[] model,float[] mvpMatrix){
        gridView.draw(model,mvpMatrix);
        float[] tmp = model.clone();
        Matrix.translateM(tmp, 0, Square.SquareSize * currentTetrimino.getTetrimino().getBlockx(), -Square.SquareSize * currentTetrimino.getTetrimino().getBlocky(), 0);
        currentTetrimino.draw(tmp,mvpMatrix);
        tmp = model.clone();
        Matrix.translateM(tmp, 0, Square.SquareSize*(Grid.gridWidth+2)-(Square.SquareSize*nextTetrimino.getTetrimino().getSize())/2, -(Square.SquareSize*Grid.gridHeight)/2+(Square.SquareSize*nextTetrimino.getTetrimino().getSize())/2, 0);
        nextTetrimino.drawMiniature(tmp,mvpMatrix);
    }

    public Game getGame(){
        return game;
    }

    @Override
    public void update(Observable observable, Object data) {
        currentTetrimino.setTetrimino(game.getCurrent());
        nextTetrimino.setTetrimino(game.getNext());
    }
}
