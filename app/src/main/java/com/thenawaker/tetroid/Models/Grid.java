package com.thenawaker.tetroid.Models;

import com.thenawaker.tetroid.Models.Tetrimino.ShapeID;

/**
 * Created by thenawaker on 20/04/16.
 */
public class Grid {
    public final static int gridWidth = 10;
    public final static int gridHeight = 22;
    private ShapeID[][] grid = new ShapeID[gridHeight][gridWidth];//Tetris Guideline size


    public Grid(){
        for(int i =0;i<gridHeight;i++){
            for(int j=0;j<gridWidth;j++){
                grid[i][j]= ShapeID.X;
            }
        }
    }

    public void put(Tetrimino tetrimino){
        for(int i=0;i<tetrimino.getSize();i++){
            for(int j=0;j<tetrimino.getSize();j++){
                if(tetrimino.getBlockx()+j<gridWidth && tetrimino.getBlocky()+i<gridHeight && tetrimino.getBlocky()+i>=0 && tetrimino.getBlockx()+j>=0 && grid[tetrimino.getBlocky()+i][tetrimino.getBlockx()+j]== Tetrimino.ShapeID.X) {
                    if (tetrimino.getShape().shape[i][j] == 'X')
                        grid[tetrimino.getBlocky()+i][tetrimino.getBlockx()+j]=tetrimino.getShapeId();
                }
            }
        }
    }

    public boolean checkCollide(Tetrimino current){
        if(current.getBlockx()+current.getShape().getFirstCol()<0)
            return true;
        if(current.getBlocky()+current.getShape().getLastRow()>=gridHeight)
            return true;
        else if(current.getBlockx()+current.getShape().getLastCol()>=gridWidth)
            return true;
        else{
            for(int i=0;i<current.getSize();i++){
                for(int j=0;j<current.getSize();j++){
                    if(current.getBlocky()+i<gridHeight && current.getBlockx()+j<gridWidth && current.getBlocky()+i>=0 && current.getBlockx()+j >=0 && grid[current.getBlocky()+i][current.getBlockx()+j]!=ShapeID.X) {
                        if (current.getShape().shape[i][j] == 'X')
                            return true;
                    }
                }
            }
        }
        return false;
    }

    public void deleteLines(){
        for (int i=0;i<gridHeight;i++){
            boolean lineComplete = true;
            for(int j=0;j<gridWidth;j++){
                if(grid[i][j]==ShapeID.X){
                    lineComplete = false;
                    break;
                }
            }
            if(lineComplete){
                moveLines(i);
            }
        }
    }

    private void moveLines(int lineid){
        for(int i=lineid;i>=0;i--){
            for(int j=0;j<gridWidth;j++){
                if(i-1>0){
                    grid[i][j]=grid[i-1][j];
                }else{
                    grid[i][j]=ShapeID.X;
                }
            }
        }
    }

    public final ShapeID[][] getGrid(){
        return grid;
    }

}
