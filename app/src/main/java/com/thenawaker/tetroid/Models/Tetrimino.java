package com.thenawaker.tetroid.Models;

import android.graphics.Color;

import java.util.HashMap;

/**
 * Created by thenawaker on 20/04/16.
 */

public class Tetrimino {

    public Tetrimino(ShapeID shape){
        setShape(shape);
    }

    public int getBlockx() {
        return blockx;
    }

    public void setBlockx(int blockx) {
        this.blockx = blockx;
    }

    public int getBlocky() {
        return blocky;
    }

    public void setBlocky(int blocky) {
        this.blocky = blocky;
    }

    public Shape getShape() {
        return shape;
    }

    public int getColor() {
        return this.shape.color;
    }

    public void setShape(ShapeID shapeid) {
        this.shapeid = shapeid;
        this.shape = shapeList.get(shapeid).clone();
    }

    public ShapeID getShapeId(){
        return shapeid;
    }

    public int getSize(){
       return getShape().shape.length;
    }

    public void rotate(Shape.Rotation rot){
        shape.rotate(rot);
    }

    public static final HashMap<ShapeID,Shape> getShapeList(){
        return shapeList;
    }

    private static HashMap<ShapeID,Shape> initShapeList(){
        HashMap<ShapeID,Shape> shapeLst = new HashMap<>();
        shapeLst.put(ShapeID.O,new Shape(new char[][]{{'X','X'},{'X','X'}}, Color.YELLOW));
        shapeLst.put(ShapeID.L,new Shape(new char[][]{{' ','X',' '},{' ','X',' '},{' ','X','X'}}, Color.rgb(255,153,0)));
        shapeLst.put(ShapeID.J,new Shape(new char[][]{{' ','X',' '},{' ','X',' '},{'X','X',' '}}, Color.BLUE));
        shapeLst.put(ShapeID.T,new Shape(new char[][]{{' ','X',' '},{'X','X','X'},{' ',' ',' '}}, Color.rgb(255,0,255)));
        shapeLst.put(ShapeID.I,new Shape(new char[][]{{' ',' ','X',' '},{' ',' ','X',' '},{' ',' ','X',' '},{' ',' ','X',' '}}, Color.rgb(0,255,255)));
        shapeLst.put(ShapeID.Z,new Shape(new char[][]{{'X','X',' '},{' ','X','X'},{' ',' ',' '}}, Color.RED));
        shapeLst.put(ShapeID.S,new Shape(new char[][]{{' ','X','X'},{'X','X',' '},{' ',' ',' '}}, Color.GREEN));
        return shapeLst;
    }

    private int blockx;
    private int blocky;
    private ShapeID shapeid;
    private Shape shape;

    private static HashMap<ShapeID,Shape> shapeList = initShapeList();
    public enum ShapeID {X,O,L,T,I,Z,S,J,SIZE}
}
