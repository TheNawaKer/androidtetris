package com.thenawaker.tetroid.Models;

/**
 * Created by thenawaker on 20/04/2016.
 */
public class Shape implements Cloneable{
    public enum Rotation{LEFT,RIGHT}
    public int color;
    public char[][] shape;

    public Shape(char[][] shape,int color){
        this.shape = shape;
        this.color = color;
    }

    public int getFirstCol(){
        for(int j=0;j<shape[0].length;j++) {
            for (int i = 0; i < shape.length; i++) {
                if(shape[i][j] == 'X')
                    return j;
            }
        }
        return 0;
    }

    public int getLastCol(){
        for(int j=shape[0].length-1;j>=0;j--) {
            for (int i = 0; i < shape.length; i++) {
                if(shape[i][j] == 'X')
                    return j;
            }
        }
        return shape[0].length-1;
    }

    public int getFirstRow(){
        for (int i = 0; i < shape.length; i++) {
        for(int j=0;j<shape[0].length;j++) {
                if(shape[i][j] == 'X')
                    return i;
            }
        }
        return 0;
    }

    public int getLastRow(){
        for (int i = shape.length-1; i>=0; i--) {
            for(int j=0;j<shape[0].length;j++) {
                if(shape[i][j] == 'X')
                    return i;
            }
        }
        return shape.length-1;
    }

    public void rotate(Rotation rot){
        char[][] finalShape = new char[shape.length][shape.length];
        switch(rot){
            case RIGHT:
                for(int i=0;i<shape.length;i++){
                    for(int j=0;j<shape[0].length;j++){
                        finalShape[j][i] = shape[i][j];
                    }
                }
                break;
            case LEFT:
                for(int i=0;i<shape.length;i++){
                    for(int j=0;j<shape[0].length;j++){
                        finalShape[j][shape[0].length-i-1] = shape[i][j];
                    }
                }
                break;
        }
        shape = finalShape;
    }

    public Shape clone(){
        Shape clone = null;
        try {
            clone = (Shape)super.clone();
        clone.color = color;
        clone.shape = shape.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return clone;
    }
}
