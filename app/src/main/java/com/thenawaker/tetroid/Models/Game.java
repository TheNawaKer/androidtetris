package com.thenawaker.tetroid.Models;

import java.util.Observable;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Semaphore;

/**
 * Created by thenawaker on 16/05/2016.
 */
public class Game extends Observable {
    private Tetrimino current;
    private Tetrimino next;
    private Grid grid;
    private Semaphore semaphore = new Semaphore(1);
    private Random rand = new Random();
    private boolean endGame = false;
    public enum Action{MOVELEFT,MOVERIGHT,ROTATE,MOVEDOWN,NOTHING};
    private Timer timer;
    private TimerTask task;
    private int delay = 500;
    private boolean down = false;

    public Game(){
        grid = new Grid();
        current = getNewTetrimino();
        next = getNewTetrimino();
        reScheduleTimer(delay);
    }

    public final Tetrimino getCurrent(){
        return current;
    }

    public final Tetrimino getNext(){
        return next;
    }

    public final Grid getGrid(){
        return grid;
    }

    private Tetrimino getNewTetrimino(){
        int val = rand.nextInt(Tetrimino.ShapeID.SIZE.ordinal()-1);
        Tetrimino tetrimino = new Tetrimino(Tetrimino.ShapeID.values()[val+1]);
        tetrimino.setBlockx(grid.gridWidth/2-tetrimino.getSize()/2);
        tetrimino.setBlocky(-tetrimino.getSize());
        return tetrimino;
    }

    public void nextFrame(){
        try {
            semaphore.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        current.setBlocky(current.getBlocky()+1);
        if(grid.checkCollide(current)){
            current.setBlocky(current.getBlocky()-1);
            if(!grid.checkCollide(current) && current.getBlocky()!=-2) {
                grid.put(current);
                grid.deleteLines();
                current = next;
                next = getNewTetrimino();
                setChanged();
                notifyObservers();
            }else{
                System.out.println("GameOver partie terminé");
                endGame = true;
            }
        }
        semaphore.release();
    }

    public void action(Action act){
        try {
            semaphore.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        switch (act){
            case MOVELEFT:
                if(down){
                    reScheduleTimer(delay);
                    down = false;
                }
                current.setBlockx(current.getBlockx()-1);
                if(grid.checkCollide(current))
                    current.setBlockx(current.getBlockx()+1);
                break;
            case MOVERIGHT:
                if(down){
                    reScheduleTimer(delay);
                    down = false;
                }
                current.setBlockx(current.getBlockx()+1);
                if(grid.checkCollide(current))
                    current.setBlockx(current.getBlockx()-1);
                break;
            case ROTATE:
                if(down){
                    reScheduleTimer(delay);
                    down = false;
                }
                current.rotate(Shape.Rotation.LEFT);
                if(grid.checkCollide(current))
                    current.rotate(Shape.Rotation.RIGHT);
                break;
            case MOVEDOWN:
                if(!down) {
                    down = true;
                    reScheduleTimer(delay / 4);
                }
                break;
            default:
                if(down){
                    reScheduleTimer(delay);
                    down = false;
                }
                break;
        }
        semaphore.release();
    }

    private void reScheduleTimer(int duration) {
        if(task!=null) {
            task.cancel();
            timer.cancel();
        }
        timer = new Timer(true);
        task = new TimerTask() {
            @Override
            public void run() {
                nextFrame();
            }
        };
        timer.scheduleAtFixedRate(task, 0, duration);
        if(duration==delay)
            System.out.println("NORMALSPEED");
        else
            System.out.println("SUPERSPEED");
    }
}
